function [evidence, NormalGammaParam] = FBST_MVD(X,etha, NptsInt)

boolVector = 0;
if (~isscalar(etha) && isvector(etha))
    boolVector = 1;
end,
if nargin < 3
    NptsInt = 500;
end,
% Cálculo do FBST para média de X = etha com variancia desconhecida

% =========================================================================
% === Médias com variancia desconhecida ===================================
% =========================================================================

if (boolVector == 0)
    n = length(X);
    Q = (n-1)*var(X);
    c_lin = (Q^(0.5*(n-1)))*sqrt(n)/ ( (2^(0.5*n)) *gamma(0.5)*gamma(0.5*(n-1)) );
    
    NormalGammaParam = struct;
    % -------------------------------------------------------------------------
    NormalGammaParam.Xmed    = mean(X);
    NormalGammaParam.Q       = Q;
    NormalGammaParam.s       = var(X);
    NormalGammaParam.n       = n;
    NormalGammaParam.c_lin   = c_lin;
    NormalGammaParam.RhoS    = (n-2)/Q;
    NormalGammaParam.ErrPad  = sqrt(NormalGammaParam.s/NormalGammaParam.n);
    % -------------------------------------------------------------------------
    % Calculo dos limites da função Normal_Gamma
    % -------------------------------------------------------------------------
    Xmed    = NormalGammaParam.Xmed;
    Rho_A = (n-2)/(Q*(1 + (n/Q)*(etha - Xmed)^2) );
    NormalGammaParam.Rho_A = Rho_A;
    NormalGammaParam.Rho_B = Rho_A;
    alpha   = -(n-2)/Q;
    beta    =  ((n-2)/Q)*log(Rho_A) - (Rho_A)*(1 + (n/Q)*(etha - Xmed)^2);
    exp_lambertW = exp(- beta/alpha)/alpha;
    Rho_C = exp( - (alpha*lambertw(exp_lambertW) + beta)/alpha );
    Rho_D = exp( - (alpha*lambertw(-1,exp_lambertW) + beta)/alpha );
    NormalGammaParam.Rho_C = abs(double(Rho_C));
    NormalGammaParam.Rho_D = abs(double(Rho_D));
    % -------------------------------------------------------------------------
    % mu      = etha;
    % rho     = NormalGammaParam.Rho_A;
    % log_Pn_Star = 0.5*(n-2)*log(rho) - 0.5*rho*Q*(1+ (n/Q)*(mu - mX)^2);
    % NormalGammaParam.Log_Ps_C = log_Pn_Star;
    % -------------------------------------------------------------------------
    %NptsInt = 1000;
    % -------------------------------------------------------------------------
    Rho_Ini = NormalGammaParam.Rho_C;
    Rho_Fim = NormalGammaParam.Rho_D;
    vecCRho = linspace(Rho_Ini,Rho_Fim,NptsInt);
    nu_Rho  = 0.5*(n-2)*log(vecCRho./Rho_A) -(Q/2)*(vecCRho - Rho_A) + 0.5*Rho_A*n*(etha - Xmed)^2;
    ag = 0.5*(n-1);
    bg = 2/Q;
    P_12 = gampdf(vecCRho,ag,bg);
    P3 = erf(real(sqrt(nu_Rho)));
    F = (P_12).*P3;
    I = simpsom(vecCRho,F);
    evidence = I;
else
    evidence = zeros(length(etha),1);
    n = length(X);
    Q = (n-1)*var(X);
    c_lin = (Q^(0.5*(n-1)))*sqrt(n)/ ( (2^(0.5*n)) *gamma(0.5)*gamma(0.5*(n-1)) );
    NormalGammaParam = struct;
    % -------------------------------------------------------------------------
    NormalGammaParam.Xmed    = mean(X);
    NormalGammaParam.Q       = Q;
    NormalGammaParam.s       = var(X);
    NormalGammaParam.n       = n;
    NormalGammaParam.c_lin   = c_lin;
    NormalGammaParam.RhoS    = (n-2)/Q;
    NormalGammaParam.ErrPad  = sqrt(NormalGammaParam.s/NormalGammaParam.n);
    Xmed    = NormalGammaParam.Xmed;
    alpha   = -(n-2)/Q;
    for k = 1:length(etha)
        % -------------------------------------------------------------------------
        % Calculo dos limites da função Normal_Gamma
        % -------------------------------------------------------------------------    
        Rho_A = (n-2)/(Q*(1 + (n/Q)*(etha(k) - Xmed)^2) );
        NormalGammaParam.Rho_A = Rho_A;
        NormalGammaParam.Rho_B = Rho_A;
        
        beta    =  ((n-2)/Q)*log(Rho_A) - (Rho_A)*(1 + (n/Q)*(etha(k) - Xmed)^2);
        exp_lambertW = exp(- beta/alpha)/alpha;
        Rho_C = exp( - (alpha*lambertw(exp_lambertW) + beta)/alpha );
        Rho_D = exp( - (alpha*lambertw(-1,exp_lambertW) + beta)/alpha );
        NormalGammaParam.Rho_C = abs(double(Rho_C));
        NormalGammaParam.Rho_D = abs(double(Rho_D));
        % -------------------------------------------------------------------------
        % mu      = etha;
        % rho     = NormalGammaParam.Rho_A;
        % log_Pn_Star = 0.5*(n-2)*log(rho) - 0.5*rho*Q*(1+ (n/Q)*(mu - mX)^2);
        % NormalGammaParam.Log_Ps_C = log_Pn_Star;
        % -------------------------------------------------------------------------
        %NptsInt = 1000;
        % -------------------------------------------------------------------------
        Rho_Ini = NormalGammaParam.Rho_C;
        Rho_Fim = NormalGammaParam.Rho_D;
        vecCRho = linspace(Rho_Ini,Rho_Fim,NptsInt);
        nu_Rho  = 0.5*(n-2)*log(vecCRho./Rho_A) -(Q/2)*(vecCRho - Rho_A) + 0.5*Rho_A*n*(etha(k) - Xmed)^2;
        ag = 0.5*(n-1);
        bg = 2/Q;
        P_12 = gampdf(vecCRho,ag,bg);
        P3 = erf(real(sqrt(nu_Rho)));
        F = (P_12).*P3;
        I = simpsom(vecCRho,F);
        evidence(k) = I;
    end,
end,
% -------------------------------------------------------------------------