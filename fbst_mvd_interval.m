function interval = fbst_mvd_interval(LLR_TESTE,alpha, epsE, verb)

if (nargin < 4) || isempty(verb)
    verb = false;
end

if (nargin < 3) || isempty(epsE)
    epsE = 1e-6;
end

if (nargin < 2) || isempty(alpha)
    alpha = 0.05;
end

Npts = length(LLR_TESTE);
med = mean(LLR_TESTE);
dev = std(LLR_TESTE);

etha0 = med;
etha1 = med+1*dev/sqrt(Npts);
abs(etha1-med);
LRval0 = (1 - alpha) - FBST_MVD(LLR_TESTE,etha0);
LRval1 = (1 - alpha) - FBST_MVD(LLR_TESTE,etha1);

k = 0;
if (verb)
    fprintf('%02i - %5.3e,%5.3e...\n',k,LRval0, LRval1);
end
while (abs(LRval1) > epsE)
    etha2 = etha1 - LRval1*(etha1 - etha0)/(LRval1 - LRval0);
    etha0 = etha1;
    etha1 = etha2;
    LRval0 = (1 - alpha)-FBST_MVD(LLR_TESTE,etha0);
    LRval1 = (1 - alpha)-FBST_MVD(LLR_TESTE,etha1);
    k = k+1;
    if (verb)
        fprintf('%02i - %5.3e,%5.3e...\n',k,LRval0, LRval1);
    end
end
interval = abs(etha2-med);